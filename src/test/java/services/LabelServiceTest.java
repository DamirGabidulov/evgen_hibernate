package services;

import lombok.Value;
import models.Label;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.LabelRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static services.TestUtils.getLabels;

@ExtendWith(MockitoExtension.class)
class LabelServiceTest {

    @Mock
    LabelRepository labelRepository;

    @InjectMocks
    LabelService labelService;

    @BeforeEach
    public void setUp(){
        Mockito.lenient().when(labelRepository.findAll()).thenReturn(getLabels());
        Mockito.lenient().when(labelRepository.getById(2L)).thenReturn(Optional.of(getLabels().get(1)));
        Mockito.lenient().when(labelRepository.save(Label.builder().name("particular").build())).thenReturn(Label.builder().id(4L).name("particular").build());
        Mockito.lenient().when(labelRepository.update(Label.builder().id(4L).name("particular").build())).thenReturn(Label.builder().id(4L).name("remarkable").build());

        labelService.setLabelRepository(labelRepository);
    }

    @Test
    void findAll() {
        assertEquals(getLabels(), labelService.findAll());
    }

    @ParameterizedTest
    @ValueSource(longs = 2L)
    void getById(Long id){
        assertEquals(Optional.of(getLabels().get(1)), labelService.getById(id));
    }

    @Test
    void save(){
        assertEquals(Label.builder().id(4L).name("particular").build(), labelService.save(Label.builder().name("particular").build()));
    }

    @Test
    void update(){
        assertEquals(Label.builder().id(4L).name("remarkable").build(), labelService.update(Label.builder().id(4L).name("particular").build()));
    }

    @Test
    void delete(){
        Label label = Label.builder().id(5L).name("outdated").build();
        labelService.delete(label.getId());
        Mockito.verify(labelRepository).deleteById(label.getId());
    }
}