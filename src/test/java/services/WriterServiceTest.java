package services;

import models.Writer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.WriterRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static services.TestUtils.*;

@ExtendWith(MockitoExtension.class)
class WriterServiceTest {

    @Mock
    WriterRepository writerRepository;

    @InjectMocks
    WriterService writerService;

    @BeforeEach
    void setUp() {
        Mockito.lenient().when(writerRepository.findAll()).thenReturn(getWriters());
        Mockito.lenient().when(writerRepository.getById(2L)).thenReturn(Optional.of(getWriters().get(1)));
        Mockito.lenient().when(writerRepository.save(getWriterWithoutId())).thenReturn(getWriterWithId());
        Mockito.lenient().when(writerRepository.update(getWriterWithId())).thenReturn(
                Writer.builder().id(3L).firstName("John").lastName("Knoxville").posts(getPosts()).build()
        );

        writerService.setWriterRepository(writerRepository);
    }

    @Test
    void findAll() {
        assertEquals(getWriters(), writerService.findAll());
    }

    @ParameterizedTest
    @ValueSource(longs = 2L)
    void getById(Long id){
        assertEquals(Optional.of(getWriters().get(1)), writerService.getById(id));
    }

    @Test
    void save(){
        assertEquals(getWriterWithId(), writerService.save(getWriterWithoutId()));
    }

    @Test
    void update(){
        assertEquals(Writer.builder().id(3L).firstName("John").lastName("Knoxville").posts(getPosts()).build(), writerService.update(getWriterWithId()));
    }

    @Test
    void deleteBuId(){
        Writer writer = getWriterWithId();
        writerService.deleteById(writer.getId());
        Mockito.verify(writerRepository).deleteById(writer.getId());
    }
}