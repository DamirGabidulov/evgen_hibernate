package services;

import models.Post;
import models.PostStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.PostRepository;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static services.TestUtils.*;

@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    @Mock
    PostRepository postRepository;

    @InjectMocks
    PostService postService;

    @BeforeEach
    void setUp(){
        Mockito.lenient().when(postRepository.findAll()).thenReturn(getPosts());
        Mockito.lenient().when(postRepository.getById(2L)).thenReturn(Optional.of(getPosts().get(1)));
        Mockito.lenient().when(postRepository.save(getPostWithoutId())).thenReturn(getPostWithId());
        Mockito.lenient().when(postRepository.update(getPostWithId())).thenReturn(Post.builder()
                .id(3L)
                .content("Books for study")
                .created(LocalDateTime.of(2021,9, 10, 12,30))
                .updated(LocalDateTime.of(2021,9, 16, 11,20))
                .status(PostStatus.ACTIVE)
                .labels(getLabels())
                .build());

        postService.setPostRepository(postRepository);
    }

    @Test
    void findAll() {
        assertEquals(getPosts(), postService.findAll());
    }

    @ParameterizedTest
    @ValueSource(longs = 2L)
    void getById(Long id){
        assertEquals(Optional.of(getPosts().get(1)), postService.getById(id));
    }

    @Test
    void save(){
        assertEquals(getPostWithId(), postService.save(getPostWithoutId()));
    }

    @Test
    void update(){
        assertEquals(Post.builder()
                .id(3L)
                .content("Books for study")
                .created(LocalDateTime.of(2021,9, 10, 12,30))
                .updated(LocalDateTime.of(2021,9, 16, 11,20))
                .status(PostStatus.ACTIVE)
                .labels(getLabels())
                .build(), postService.update(getPostWithId()));
    }
}