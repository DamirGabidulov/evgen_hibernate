package services;

import models.Label;
import models.Post;
import models.PostStatus;
import models.Writer;

import java.time.LocalDateTime;
import java.util.List;

public class TestUtils {

    public static List<Label> getLabels(){
        return List.of(
                Label.builder().id(1L).name("breaking").build(),
                Label.builder().id(2L).name("disappointed").build(),
                Label.builder().id(3L).name("gorgeous").build());
    }

    public static List<Post> getPosts(){
        return List.of(
                Post.builder()
                        .id(1L)
                        .content("Travel around the World")
                        .created(LocalDateTime.of(2023,2, 20, 12,30))
                        .updated(LocalDateTime.of(2023,2, 26, 11,20))
                        .status(PostStatus.ACTIVE)
                        .labels(getLabels())
                .build(),
                Post.builder()
                        .id(2L)
                        .content("PS5 games")
                        .created(LocalDateTime.of(2022,5, 20, 12,30))
                        .updated(LocalDateTime.of(2022,5, 26, 11,20))
                        .status(PostStatus.ACTIVE)
                        .labels(getLabels())
                        .build()
        );
    }

    public static Post getPostWithId(){
        return Post.builder()
                .id(3L)
                .content("Books for kids")
                .created(LocalDateTime.of(2021,9, 10, 12,30))
                .updated(LocalDateTime.of(2021,9, 16, 11,20))
                .status(PostStatus.ACTIVE)
                .labels(getLabels())
                .build();
    }

    public static Post getPostWithoutId(){
        return Post.builder()
                .content("Books for kids")
                .created(LocalDateTime.of(2021,9, 10, 12,30))
                .updated(LocalDateTime.of(2021,9, 16, 11,20))
                .status(null)
                .labels(getLabels())
                .build();
    }

    public static List<Writer> getWriters(){
        return List.of(
                Writer.builder().id(1L).firstName("Michael").lastName("Scofield").posts(getPosts()).build(),
                Writer.builder().id(2L).firstName("Lincoln").lastName("Burrows").posts(getPosts()).build()
        );
    }

    public static Writer getWriterWithoutId(){
        return Writer.builder().firstName("Johnny").lastName("Knoxville").posts(getPosts()).build();
    }

    public static Writer getWriterWithId(){
        return Writer.builder().id(3L).firstName("Johnny").lastName("Knoxville").posts(getPosts()).build();
    }
}
