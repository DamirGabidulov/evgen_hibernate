package views;

import java.util.Scanner;

public class GeneralView {

    private final LabelView labelView;
    private final PostView postView;

    private final WriterView writerView;

    public GeneralView() {
        this.labelView = new LabelView();
        this.postView = new PostView();
        this.writerView = new WriterView();
    }

    Scanner scanner = new Scanner(System.in);

    public void start(){
        while (true){
            System.out.println("\n -------------------------- \n");
            System.out.println("Выберите с чем хотите работать:");
            System.out.println("Писатель - введите writer");
            System.out.println("Пост - введите post");
            System.out.println("Тег - введите label");
            String action = scanner.nextLine();
            doAction(action);
        }
    }

    String command = null;

    private void doAction(String action) {

        switch (action) {
            case "writer":
                System.out.println("Сохранить нового писателя - введите 1");
                System.out.println("Выбрать всех писателей - введите 2");
                System.out.println("Выбрать писателя по id - введите 3");
                System.out.println("Обновить данные у писателя - введите 4");
                System.out.println("Удалить писателя по id - введите 5");
                command = scanner.nextLine();
                writerView.doAction(command);
                break;
            case "post":
                System.out.println("Сохранить новый пост - введите 1");
                System.out.println("Выбрать все посты - введите 2");
                System.out.println("Выбрать пост по id - введите 3");
                System.out.println("Обновить данные у поста - введите 4");
                System.out.println("Удалить пост по id - введите 5");
                System.out.println("Отправить пост на проверку по id - введите 6");
                command = scanner.nextLine();
                postView.doAction(command);
                break;
            case "label":
                System.out.println("Сохранить новый тег - введите 1");
                System.out.println("Выбрать все теги - введите 2");
                System.out.println("Выбрать тег по id - введите 3");
                System.out.println("Обновить данные у тега - введите 4");
                System.out.println("Удалить тег по id - введите 5");
                command = scanner.nextLine();
                labelView.doAction(command);
                break;

        }
    }
}
