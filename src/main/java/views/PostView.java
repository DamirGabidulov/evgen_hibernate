package views;

import controllers.LabelController;
import controllers.PostController;

import java.util.Scanner;

public class PostView {

    private final PostController postController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;

    public PostView() {
        this.postController = new PostController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите контент");
                data = scanner.nextLine();
                save(data);
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id поста который хотите обновить");
                String postId = scanner.nextLine();
                getById(postId);
                System.out.println("Если хотите обновить контент введите - 1");
                System.out.println("Если хотите добавить теги к посту - 2");
                System.out.println("Если хотите удалить теги у поста - 3");
                data = scanner.nextLine();
                if (data.equals("1")){
                    System.out.println("Введите новый контент");
                    data = scanner.nextLine();
                    updateContent(postId, data);
                } else if (data.equals("2")){
                    System.out.println("Введите теги через запятую");
                    data = scanner.nextLine();
                    addLabels(postId, data);
                    getById(postId);
                } else if (data.equals("3")){
                    System.out.println("Введите айди тегов через запятую");
                    data = scanner.nextLine();
                    deleteLabels(postId, data);
                    getById(postId);
                }
                break;
            case "5":
                System.out.println("Введите id поста который хотите удалить");
                data = scanner.nextLine();
                delete(data);
                System.out.println("Пост с id " + data + " удален");
                break;
            case "6":
                System.out.println("Введите id поста который хотите отправить на проверку");
                data = scanner.nextLine();
                getById(data);
                System.out.println("Отправляем пост на проверку модератором...");
                checkPost(data);
                System.out.println("Пост проверен, статус Active");
                break;
        }
    }

    private void findAll(){
        System.out.println(postController.findAll());
    }

    private void getById(String data){
        System.out.println(postController.getById(data));
    }

    private void save(String data){
        System.out.println(postController.save(data));
    }

    private void checkPost(String data){
        System.out.println(postController.checkPost(data));
    }

    private void updateContent(String postId, String data){
        System.out.println(postController.updateContent(postId, data));
    }

    private void addLabels(String postId, String data){
        postController.addLabels(postId, data);
    }

    private void deleteLabels(String postId, String data){
        postController.deleteLabels(postId, data);
    }

    private void delete(String data){
        postController.delete(data);
    }
}
