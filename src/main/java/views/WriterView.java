package views;

import controllers.PostController;
import controllers.WriterController;
import models.Post;
import models.Writer;

import java.util.Scanner;

public class WriterView {

    private final WriterController writerController;
    private final PostController postController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;

    public WriterView() {
        this.writerController = new WriterController();
        this.postController = new PostController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите имя и фамилию писателя через запятую");
                data = scanner.nextLine();
                Writer writer = save(data);
                System.out.println(writer);
                System.out.println("Если хотите добавить новый пост введите - 1, если нет - любой символ");
                data = scanner.nextLine();
                if (data.equals("1")) {
                    System.out.println("Введите новый пост");
                    data = scanner.nextLine();
                    Post post = addNewPostToWriter(writer.getId(), data);
                    getById(writer.getId().toString());
                    System.out.println("Если хотите добавить теги к посту введите - 1, если нет - любой символ");
                    data = scanner.nextLine();
                    if (data.equals("1")) {
                        System.out.println("Введите теги через запятую");
                        data = scanner.nextLine();
                        postController.addLabels(post.getId().toString(), data);
                        getById(writer.getId().toString());
                    }
                }
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id писателя который хотите обновить");
                String writerId = scanner.nextLine();
                getById(writerId);
                System.out.println("Если хотите обновить имя/фамилию писателя введите - 1");
                System.out.println("Если хотите добавить новый пост писателю введите - 2");
                System.out.println("Если хотите удалить посты у писателя - 3");
                data = scanner.nextLine();
                if (data.equals("1")) {
                    System.out.println("Введите новое имя и фамилию через запятую");
                    data = scanner.nextLine();
                    updateWriterFullName(writerId, data);
                }
                else if (data.equals("2")) {
                    System.out.println("Введите новый пост");
                    data = scanner.nextLine();
                    Post newPost = addNewPostToWriter(Long.valueOf(writerId), data);
                    getById(writerId);
                    System.out.println("Если хотите добавить теги к посту введите - 1, если нет - любой символ");
                    data = scanner.nextLine();
                    if (data.equals("1")) {
                        System.out.println("Введите теги через запятую");
                        data = scanner.nextLine();
                        postController.addLabels(newPost.getId().toString(), data);
                        getById(writerId);
                    }
                }
                 else if (data.equals("3")) {
                    System.out.println("Введите айди постов через запятую");
                    data = scanner.nextLine();
                    deletePostsFromWriter(data);
                    getById(writerId);
                }
                break;
            case "5":
                System.out.println("Введите id писателя которого хотите удалить");
                data = scanner.nextLine();
                delete(data);
                break;
        }
    }

    private void findAll() {
        System.out.println(writerController.findAll());
    }

    private void getById(String data) {
        System.out.println(writerController.getById(data));
    }

    private Writer save(String data) {
        return writerController.save(data);
    }

    private Post addNewPostToWriter(Long writerId, String postContent) {
        return writerController.addNewPostToWriter(writerId, postContent);
    }

    private void updateWriterFullName(String writerId, String data) {
        System.out.println(writerController.updateWriterFullName(writerId, data));
    }

    private void deletePostsFromWriter(String data) {
        writerController.deletePostsFromWriterByIds(data);
    }

    private void delete(String data){
        writerController.delete(data);
        System.out.println("Deleted");
    }
}
