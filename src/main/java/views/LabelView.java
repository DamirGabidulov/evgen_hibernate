package views;

import controllers.LabelController;

import java.util.Scanner;

public class LabelView {

    private final LabelController labelController;
    private Scanner scanner = new Scanner(System.in);
    private String data = null;

    public LabelView() {
        this.labelController = new LabelController();
    }

    public void doAction(String command) {
        switch (command) {
            case "1":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                save(data);
                break;
            case "2":
                findAll();
                break;
            case "3":
                System.out.println("Введите данные");
                data = scanner.nextLine();
                getById(data);
                break;
            case "4":
                System.out.println("Введите id тега который хотите обновить");
                String tagId = scanner.nextLine();
                getById(tagId);
                System.out.println("Введите новое имя");
                data = scanner.nextLine();
                update(tagId, data);
                break;
            case "5":
                System.out.println("Введите id тега который хотите удалить");
                data = scanner.nextLine();
                delete(data);
                break;
        }
    }

    private void findAll(){
        System.out.println(labelController.findAll());
    }

    private void getById(String data){
        System.out.println(labelController.getById(data));
    }

    private void save(String data){
        System.out.println(labelController.save(data));
    }

    private void update(String tagId, String data) {
        System.out.println(labelController.update(tagId, data));
    }

    private void delete(String data) {
        labelController.delete(data);
    }
}
