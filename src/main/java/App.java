import views.GeneralView;

public class App {
    public static void main(String[] args) {
        GeneralView generalView = new GeneralView();
        generalView.start();
    }
}
