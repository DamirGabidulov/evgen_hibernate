package models;

public enum PostStatus {
    ACTIVE, UNDER_REVIEW, DELETED
}
