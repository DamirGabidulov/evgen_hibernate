package services;

import models.Writer;
import repositories.WriterRepository;
import repositories.impl.WriterRepositoryImpl;

import java.util.List;
import java.util.Optional;

public class WriterService {

    private WriterRepository writerRepository;

    public WriterService() {
        this.writerRepository = new WriterRepositoryImpl();
    }

    public void setWriterRepository(WriterRepository writerRepository) {
        this.writerRepository = writerRepository;
    }

    public List<Writer> findAll() {
       return writerRepository.findAll();
    }

    public Optional<Writer> getById(Long writerId) {
        return writerRepository.getById(writerId);
    }

    public Writer save(Writer writer) {
        return writerRepository.save(writer);
    }

    public Writer update(Writer writer) {
        return writerRepository.update(writer);
    }

    public void deleteById(Long writerId) {
        writerRepository.deleteById(writerId);
    }
}
