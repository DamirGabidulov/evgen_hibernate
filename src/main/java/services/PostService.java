package services;

import controllers.PostController;
import models.Post;
import repositories.PostRepository;
import repositories.impl.PostRepositoryImpl;

import java.util.List;
import java.util.Optional;

public class PostService {

    private PostRepository postRepository;

    public PostService() {
        this.postRepository = new PostRepositoryImpl();
    }

    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> findAll() {
     return postRepository.findAll();
    }

    public Optional<Post> getById(Long postId) {
     return postRepository.getById(postId);
    }

    public Post save(Post post) {
        return postRepository.save(post);
    }

    public Post update(Post post) {
        return postRepository.update(post);
    }
}
