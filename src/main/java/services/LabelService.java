package services;

import models.Label;
import repositories.LabelRepository;
import repositories.impl.LabelRepositoryImpl;

import java.util.List;
import java.util.Optional;

public class LabelService {

    private LabelRepository labelRepository;

    public LabelService() {
        this.labelRepository = new LabelRepositoryImpl();
    }

    public void setLabelRepository(LabelRepository labelRepository){
        this.labelRepository = labelRepository;
    }

    public List<Label> findAll() {
        return labelRepository.findAll();
    }

    public Optional<Label> getById(Long id) {
        return labelRepository.getById(id);
    }

    public Label save(Label label) {
        return labelRepository.save(label);
    }

    public Label update(Label label) {
        return labelRepository.update(label);
    }

    public void delete(Long labelId) {
        labelRepository.deleteById(labelId);
    }
}
