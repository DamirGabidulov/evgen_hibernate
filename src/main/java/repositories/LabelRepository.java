package repositories;

import models.Label;

import java.util.List;
import java.util.Optional;

public interface LabelRepository extends GenericRepository<Label, Long> {

    @Override
    List<Label> findAll();

    @Override
    Optional<Label> getById(Long id);

    @Override
    Label save(Label label);

    @Override
    Label update(Label label);

    @Override
    void deleteById(Long id);
}
