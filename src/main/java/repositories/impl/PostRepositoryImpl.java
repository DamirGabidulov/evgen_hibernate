package repositories.impl;

import models.Post;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repositories.PostRepository;

import java.util.List;
import java.util.Optional;

import static utils.HibernateUtils.getSession;

public class PostRepositoryImpl implements PostRepository {

//        private static final String SQL_FIND_ALL = "select post from Post post left join fetch post.labels";
    private static final String SQL_FIND_ALL = "select post from Post post";
    private static final String SQL_GET_BY_ID = "select post from Post post left join fetch post.labels where post.id = :id";


    @Override
    public List<Post> findAll() {
        try (Session session = getSession()) {
                Query<Post> posts = session.createQuery(SQL_FIND_ALL, Post.class);
                return posts.getResultList();
        }
    }

    @Override
    public Optional<Post> getById(Long id) {
        try (Session session = getSession()) {
                Query<Post> post = session.createQuery(SQL_GET_BY_ID, Post.class);
                post.setParameter("id", id);
                if (post.getResultList().isEmpty()) {
                    return Optional.empty();
                } else {
                    return Optional.of(post.getSingleResult());
            }
        }
    }

    @Override
    public Post save(Post post) {
        try (Session session = getSession()) {
                session.beginTransaction();
                session.persist(post);
                Post savedPost = session.load(Post.class, post.getId());
                session.getTransaction().commit();
                return savedPost;
        }
    }

    @Override
    public Post update(Post post) {
        try (Session session = getSession()) {
                session.beginTransaction();
                session.update(post);
                Post updatedPost = session.load(Post.class, post.getId());
                session.getTransaction().commit();
                return updatedPost;
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Session session = getSession()) {
                session.beginTransaction();
                Post expectedPost = session.load(Post.class, id);
                session.delete(expectedPost);
                session.getTransaction().commit();
        }
    }
}
