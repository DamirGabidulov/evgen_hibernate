package repositories.impl;

import models.Label;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repositories.LabelRepository;

import java.util.List;
import java.util.Optional;

import static utils.HibernateUtils.getSession;

public class LabelRepositoryImpl implements LabelRepository {

    private static final String SQL_FIND_ALL = "from Label";
    private static final String SQL_GET_BY_ID = "select label from Label label where label.id = :id";

    @Override
    public List<Label> findAll() {
        try (Session session = getSession()) {
            Query<Label> labels = session.createQuery(SQL_FIND_ALL, Label.class);
            return labels.getResultList();
        }
    }

    @Override
    public Optional<Label> getById(Long id) {
        try (Session session = getSession()) {
            Query<Label> label = session.createQuery(SQL_GET_BY_ID, Label.class);
            label.setParameter("id", id);
            if (label.getResultList().isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(label.getSingleResult());
            }
        }
    }

    @Override
    public Label save(Label label) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.persist(label);
            Label savedLabel = session.load(Label.class, label.getId());
            session.getTransaction().commit();
            return savedLabel;
        }
    }

    @Override
    public Label update(Label label) {
        try (Session session = getSession()) {
            session.beginTransaction();
            session.update(label);
            Label updatedLabel = session.load(Label.class, label.getId());
            session.getTransaction().commit();
            return updatedLabel;
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Session session = getSession()) {
            session.beginTransaction();
            Label expectedLabel = session.load(Label.class, id);
            //нужно ли самому еще раз перехватывать ошибку EntityNotFoundException если такой сущности нет в базе?
            session.delete(expectedLabel);
            session.getTransaction().commit();
        }
    }
}
