package repositories.impl;

import models.Writer;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repositories.WriterRepository;

import java.util.List;
import java.util.Optional;

import static utils.HibernateUtils.getSession;

public class WriterRepositoryImpl implements WriterRepository {

    private static final String SQL_FIND_ALL = "select writer from Writer writer";
    private static final String GET_BY_ID = "select writer from Writer writer left join fetch writer.posts where writer.id = :id";

    @Override
    public List<Writer> findAll() {
        try (Session session = getSession()) {
                Query<Writer> writers = session.createQuery(SQL_FIND_ALL, Writer.class);
                return writers.getResultList();
        }
    }

    @Override
    public Optional<Writer> getById(Long id) {
        try (Session session = getSession()) {
                Query<Writer> writer = session.createQuery(GET_BY_ID, Writer.class);
                writer.setParameter("id", id);
                if (writer.getResultList().isEmpty()) {
                    return Optional.empty();
                } else {
                    return Optional.of(writer.getSingleResult());
                }
        }
    }

    @Override
    public Writer save(Writer writer) {
        try (Session session = getSession()) {
                session.beginTransaction();
                session.saveOrUpdate(writer);
                Writer savedWriter = session.load(Writer.class, writer.getId());
                session.getTransaction().commit();
                return savedWriter;
        }
    }

    @Override
    public Writer update(Writer writer) {
        try (Session session = getSession()) {
                session.beginTransaction();
                session.update(writer);
                Writer updatedWriter = session.load(Writer.class, writer.getId());
                session.getTransaction().commit();
                return updatedWriter;
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Session session = getSession()) {
                session.beginTransaction();
                Writer expectedWriter = session.load(Writer.class, id);
                session.delete(expectedWriter);
                session.getTransaction().commit();
        }
    }
}
