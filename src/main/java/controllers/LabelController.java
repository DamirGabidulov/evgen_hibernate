package controllers;

import models.Label;
import models.Post;
import services.LabelService;
import services.PostService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LabelController {

    private final LabelService labelService;

    public LabelController() {
        this.labelService = new LabelService();
    }

    public List<Label> findAll() {
        return labelService.findAll();
    }

    public Label getById(String data) {
        if (data.matches("\\d+")){
            Long labelId = Long.valueOf(data);
            Optional<Label> optionalLabel = labelService.getById(labelId);
            return optionalLabel.orElseThrow(() -> new IllegalArgumentException("Label with this id doesn't exist"));
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Label save(String data) {
        Label label = Label.builder()
                .name(data)
                .build();
        return labelService.save(label);
    }

    public Label update(String tagId, String data) {
        if (tagId.matches("\\d+")){
            Long labelId = Long.valueOf(tagId);
            Label label = Label.builder()
                    .id(labelId)
                    .name(data)
                    .build();
            return labelService.update(label);
        }else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public void delete(String data) {
        if (data.matches("\\d+")){
            Long labelId = Long.valueOf(data);
            labelService.delete(labelId);
            System.out.println("Label with id " + labelId + " is deleted");
        }else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }
}
