package controllers;

import models.Post;
import models.PostStatus;
import models.Writer;
import services.PostService;
import services.WriterService;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class WriterController {

    private final WriterService writerService;
    private final PostService postService;

    public WriterController() {
        this.writerService = new WriterService();
        this.postService = new PostService();
    }

    public List<Writer> findAll() {
        return writerService.findAll();
    }

    public Writer getById(String data) {
        if (data.matches("\\d+")) {
            Long writerId = Long.valueOf(data);
            Optional<Writer> optionalWriter = writerService.getById(writerId);
            return optionalWriter.orElseThrow(() -> new IllegalArgumentException("Writer with this id doesn't exist"));
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }


    public Writer save(String data) {
        String[] writerFullName = data.split(",");
        Writer writer = Writer.builder()
                .firstName(writerFullName[0])
                .lastName(writerFullName[1])
                .build();
        return writerService.save(writer);
    }

    public Post addNewPostToWriter(Long writerId, String postContent) {
        Post post = Post.builder()
                .content(postContent)
                .created(LocalDateTime.now())
                .updated(LocalDateTime.now())
                .status(PostStatus.UNDER_REVIEW)
                .build();
        postService.save(post);
        Writer writer = getById(writerId.toString());
        writer.getPosts().add(post);
        writerService.update(writer);
        return post;
    }

    public Writer updateWriterFullName(String writerId, String data) {
        String[] writerFullName = data.split(",");
        Writer writer = getById(writerId);
        writer.setFirstName(writerFullName[0]);
        writer.setLastName(writerFullName[1]);
        return writerService.update(writer);
    }

    public void deletePostsFromWriterByIds(String data) {
        String[] postIds = data.split(",");
        for (String id : postIds) {
            if (id.matches("\\d+")) {
                Long postId = Long.valueOf(id);
                Optional<Post> optionalPost = postService.getById(postId);
                if (optionalPost.isPresent()) {
                    Post post = optionalPost.get();
                    post.setStatus(PostStatus.DELETED);
                    postService.update(post);
                } else {
                    throw new IllegalArgumentException("Post with this id doesn't exist");
                }
            } else {
                throw new IllegalArgumentException("Wrong number format");
            }
        }
    }

    public void delete(String data) {
        if (data.matches("\\d+")) {
            Long writerId = Long.valueOf(data);
            writerService.deleteById(writerId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }
}
