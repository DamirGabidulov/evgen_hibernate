package controllers;

import models.Label;
import models.Post;
import models.PostStatus;
import services.LabelService;
import services.PostService;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PostController {

    private final PostService postService;
    private final LabelService labelService;

    public PostController() {
        this.postService = new PostService();
        this.labelService = new LabelService();
    }

    public List<Post> findAll() {
        return postService.findAll();
    }

    public Post getById(String data) {
        if (data.matches("\\d+")) {
            Long postId = Long.valueOf(data);
            Optional<Post> optionalPost = postService.getById(postId);
            return optionalPost.orElseThrow(() -> new IllegalArgumentException("Post with this id doesn't exist"));
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    public Post save(String data) {
        Post post = Post.builder()
                .content(data)
                .created(LocalDateTime.now())
                .updated(LocalDateTime.now())
                .status(PostStatus.UNDER_REVIEW)
                .build();
        return postService.save(post);
    }

    public Post checkPost(String data) {
        Post post = getById(data);
        if (isForbidden(post)) {
            throw new IllegalStateException("Please use correct words");
        } else {
            post.setStatus(PostStatus.ACTIVE);
            post.setUpdated(LocalDateTime.now());
            postService.update(post);
            return post;
        }
    }

    private boolean isForbidden(Post post) {
        String[] forbiddenWords = {"fuck", "bitch", "suck"};
        return Arrays.stream(forbiddenWords).anyMatch(word -> post.getContent().contains(word));
    }

    public Post updateContent(String postId, String data) {
        Post post = getById(postId);
        post.setContent(data);
        post.setUpdated(LocalDateTime.now());
        return postService.update(post);
    }

    public void addLabels(String id, String data) {
        if (id.matches("\\d+")) {
            String[] labelNames = data.split(",");
            List<Label> labels = new ArrayList<>();
            for (String l : labelNames) {
                Label label = Label.builder()
                        .name(l)
                        .build();
                labelService.save(label);
                labels.add(label);
            }
            Long postId = Long.valueOf(id);
            Optional<Post> optionalPost = postService.getById(postId);
            if (optionalPost.isPresent()) {
                Post post = optionalPost.get();
                List<Label> previousLabels = post.getLabels();
                List<Label> updatedListOfLabels = Stream.of(previousLabels, labels).flatMap(Collection::stream).collect(Collectors.toList());
                post.setLabels(updatedListOfLabels);
                postService.update(post);
            } else {
                throw new IllegalArgumentException("Post with this id doesn't exist");
            }
        }
    }


    public void deleteLabels(String id, String data) {
        if (id.matches("\\d+")) {
            Long postId = Long.valueOf(id);
            Optional<Post> optionalPost = postService.getById(postId);
            if (optionalPost.isPresent()) {
                Post post = optionalPost.get();
                List<Label> postLabels = post.getLabels();

                String[] labelIds = data.split(",");
                List<Label> labelsForRemove = new ArrayList<>();
                for (String l : labelIds) {
                    if (l.matches("\\d+")){
                        Long labelId = Long.valueOf(l);
                        Label labelFromBlackList = postLabels.stream().filter(label -> label.getId().equals(labelId)).findFirst().get();
                        labelsForRemove.add(labelFromBlackList);
                    } else {
                        throw new IllegalArgumentException("Wrong number format");
                    }
                }
                post.getLabels().removeAll(labelsForRemove);
                postService.update(post);

            } else {
                throw new IllegalArgumentException("Post with this id doesn't exist");
            }
        }
    }

    public void delete(String data) {
        Post post = getById(data);
        post.setStatus(PostStatus.DELETED);
        postService.update(post);
    }
}
