create table if not exists writer
(
    id bigserial primary key,
    firstname varchar(255),
    lastname  varchar(255)
);