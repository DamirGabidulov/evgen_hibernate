create table if not exists post_label
(
    post_id  bigint not null,
    foreign key (post_id) references post(id),
    label_id bigint not null,
    foreign key (label_id) references label(id)
);