create table if not exists post
(
    id bigserial primary key,
    content varchar(255),
    created timestamp,
    updated timestamp,
    post_status varchar(255),
    writer_id bigint,
    foreign key (writer_id) references writer (id)
);